import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import "./index.scss";
import registerServiceWorker from "./registerServiceWorker";

import reducer from "./redux/reducer";
import { initialState } from "./redux/State";
import Main from "./components/Main";
import fetchData from "./fetchData";
import createAction from "./redux/actions/createAction";

const store = createStore(reducer, initialState);

const loadData = async () => {
	const data = await fetchData();
	store.dispatch(
		createAction(state => ({
			...state,
			...data,
			isLoading: false,
			cars: data.cars.map(car => ({
				...car,
				isSelected: false,
				position: 0
			})),
			traffic_lights: data.traffic_lights.map(light => ({
				...light,
				green: false
			}))
		}))
	);
};

ReactDOM.render(
	<Provider store={store}>
		<Main />
	</Provider>,
	document.getElementById("root") as HTMLElement
);

loadData();

registerServiceWorker();
