import updateCarPosition from "./updateCarPosition";
import State, { Car } from "../redux/State";

const car: Car = {
	position: 0,
	description: "",
	id: 1,
	speed: 10,
	image: "",
	isSelected: true,
	name: ""
};

const state: State = {
	distance: 100,
	animationSpeed: 1,
	cars: [car],
	isLoading: false,
	searchValue: "",
	speed_limits: [],
	started: true,
	traffic_lights: []
};

const HOUR = 60 * 60 * 1000;

it("update car position", () => {
	let updatedCar = updateCarPosition(car, state, HOUR);
	expect(updatedCar.position).toBe(car.speed);
	expect(updatedCar.finishPosition).toBeFalsy();

	updatedCar = updateCarPosition(car, state, 10 * HOUR);
	expect(updatedCar.position).toBe(car.speed * 10);
	expect(updatedCar.finishPosition).toBe(1);

	state.traffic_lights = [{ position: 50, green: false, duration: HOUR }];
	updatedCar = updateCarPosition(car, state, HOUR * 10);
	expect(updatedCar.position).toBe(50);

	state.traffic_lights = [];
	state.speed_limits = [{ position: 50, speed: 1 }];
	updatedCar = updateCarPosition(car, state, HOUR * 10);
	expect(updatedCar.position).toBe(55);
});
