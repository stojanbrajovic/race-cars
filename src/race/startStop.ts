import { Dispatch } from "redux";
import createAction from "../redux/actions/createAction";
import resetCarPositions from "./resetCarPositions";
import updateCarPosition from "./updateCarPosition";
import { startTrafficLights, stopTrafficLights } from "./trafficLights";

let intervalId: number;
const REFRESH_RATE = 60;

const startRace = (dispatch: Dispatch) => {
	resetCarPositions(dispatch, true);
	intervalId = window.setInterval(() => {
		dispatch(
			createAction(state => ({
				...state,
				cars: state.cars.map(car =>
					updateCarPosition(car, state, REFRESH_RATE)
				)
			}))
		);
	}, 1000 / REFRESH_RATE);
	startTrafficLights(dispatch);
};

const stopRace = (dispatch: Dispatch) => {
	window.clearInterval(intervalId);
	resetCarPositions(dispatch, false);
	stopTrafficLights(dispatch);
};

export { startRace, stopRace };
