import { Dispatch } from "redux";
import createAction from "../redux/actions/createAction";

let intervalIds: number[] = [];

const clearIntervals = () => {
	intervalIds.forEach(id => window.clearInterval(id));
	intervalIds = [];
};

export const startTrafficLights = (dispatch: Dispatch) => {
	clearIntervals();
	dispatch(
		createAction(initState => {
			intervalIds = initState.traffic_lights.map((trafficLight, index) =>
				window.setInterval(() => {
					dispatch(
						createAction(state => {
							const trafficLights = state.traffic_lights.slice();
							const light = trafficLights[index];
							trafficLights[index] = {
								...light,
								green: !light.green
							};

							return {
								...state,
								traffic_lights: trafficLights
							};
						})
					);
				}, trafficLight.duration / initState.animationSpeed)
			);

			return initState;
		})
	);
};

export const stopTrafficLights = (dispatch: Dispatch) => {
	clearIntervals();
	dispatch(
		createAction(state => ({
			...state,
			traffic_lights: state.traffic_lights.map(light => ({
				...light,
				green: false
			}))
		}))
	);
};
