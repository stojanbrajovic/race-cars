import { SpeedLimit } from "../Data";
import { Car } from "../redux/State";

const getSpeedForCar = (car: Car, speedLimits: SpeedLimit[]) => {
	const { position, speed } = car;

	const initialSpeedLimit: SpeedLimit = {
		speed: Number.MAX_SAFE_INTEGER,
		position: Number.MIN_SAFE_INTEGER
	};
	const activeSpeedLimit = speedLimits.reduce((result, limit) => {
		const resultDistance = Math.abs(result.position - position);
		const distance = Math.abs(limit.position - position);
		if (position >= limit.position && distance < resultDistance) {
			return limit;
		}

		return result;
	}, initialSpeedLimit);

	return Math.min(activeSpeedLimit.speed, speed);
};

export default getSpeedForCar;
