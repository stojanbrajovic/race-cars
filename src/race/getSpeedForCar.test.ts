import getSpeedForCar from "./getSpeedForCar";
import { Car } from "../redux/State";
import { SpeedLimit } from "../Data";

it("speed on position test", () => {
	const car: Car = {
		position: 100,
		speed: 100,
		description: "",
		name: "",
		id: 1,
		image: "",
		isSelected: true
	};
	const speedLimit: SpeedLimit = { position: 90, speed: 50 };

	const getSpeed = () => getSpeedForCar(car, [speedLimit]);

	expect(getSpeed()).toBe(50);

	speedLimit.position = 200;
	expect(getSpeed()).toBe(100);

	speedLimit.position = 50;
	speedLimit.speed = 200;
	expect(getSpeed()).toBe(100);

	speedLimit.speed = 70;
	const speedLimit2: SpeedLimit = { position: 90, speed: 40 };
	expect(getSpeedForCar(car, [speedLimit, speedLimit2])).toBe(40);
});
