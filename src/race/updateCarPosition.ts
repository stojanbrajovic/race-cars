import State, { Car, TrafficLight } from "../redux/State";
import getSpeedForCar from "./getSpeedForCar";
import { SpeedLimit } from "../Data";

const HOUR = 1000 * 60 * 60;

const getClosestRedLight = (position: number, trafficLights: TrafficLight[]) =>
	trafficLights.reduce(
		(result, light) => {
			if (
				light.position >= position &&
				light.position < result.position &&
				!light.green
			) {
				return light;
			}

			return result;
		},
		{
			position: Number.MAX_SAFE_INTEGER,
			duration: Number.MAX_SAFE_INTEGER,
			green: false
		} as TrafficLight
	);

const getClosestSpeedLimit = (position: number, speedLimits: SpeedLimit[]) =>
	speedLimits.reduce(
		(result, limit) => {
			if (limit.position > position && limit.position < result.position) {
				return limit;
			}

			return result;
		},
		{
			position: Number.MAX_SAFE_INTEGER,
			speed: Number.MAX_SAFE_INTEGER
		} as SpeedLimit
	);

const updateCarPosition = (carArg: Car, state: State, timeMs: number): Car => {
	if (!carArg.isSelected || carArg.finishPosition) {
		return carArg;
	}

	let timeRemaining = timeMs;
	const car = { ...carArg };
	do {
		const speedLimit = getClosestSpeedLimit(
			car.position,
			state.speed_limits
		);
		const redLight = getClosestRedLight(car.position, state.traffic_lights);

		const speed = getSpeedForCar(car, state.speed_limits);
		const position =
			car.position +
			(speed * timeRemaining * state.animationSpeed) / HOUR;

		if (
			redLight.position < speedLimit.position &&
			position > redLight.position
		) {
			car.position = redLight.position;
			timeRemaining = 0;
		} else if (speedLimit.position < position) {
			timeRemaining -=
				((speedLimit.position - car.position) * HOUR) /
				(speed * state.animationSpeed);
			car.position = speedLimit.position;
		} else {
			car.position = position;
			timeRemaining = 0;
		}
	} while (timeRemaining > 0);

	car.position = Math.min(car.position, state.distance);

	car.finishPosition =
		car.position === state.distance
			? state.cars
					.filter(c => c !== car && c.isSelected)
					.reduce(
						(result, c) => result + (c.finishPosition ? 1 : 0),
						1
					)
			: 0;

	return car;
};

export default updateCarPosition;
