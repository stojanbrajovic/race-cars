import { Dispatch } from "redux";

import createAction from "../redux/actions/createAction";

const resetCarPositions = (dispatch: Dispatch, started: boolean) =>
	dispatch(
		createAction(state => ({
			...state,
			started,
			cars: state.cars.map(car => ({
				...car,
				position: 0,
				finishPosition: 0
			}))
		}))
	);

export default resetCarPositions;
