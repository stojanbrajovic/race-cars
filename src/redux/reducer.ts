import State from "./State";
import StoreAction from "./actions/StoreAction";

const reducer = (state: State, action: StoreAction) => {
	if (action.type === "STORE_ACTION_TYPE") {
		return action.getNextState(state);
	} else {
		return state;
	}
};

export default reducer;
