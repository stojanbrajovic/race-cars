import { Dispatch } from "redux";
import State from "../State";
import createAction from "./createAction";

const setState = (dispatch: Dispatch, newState: Partial<State>) => {
	dispatch(createAction(state => ({ ...state, ...newState })));
};

export default setState;
