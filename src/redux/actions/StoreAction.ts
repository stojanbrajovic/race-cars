import State from "../State";

export type GetNextState = (state: State) => State | Promise<State>;

interface StoreAction {
	type: "STORE_ACTION_TYPE";
	getNextState: GetNextState;
}

export default StoreAction;
