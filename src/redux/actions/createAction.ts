import StoreAction, { GetNextState } from "./StoreAction";

const createAction = (getNextState: GetNextState): StoreAction => ({
	type: "STORE_ACTION_TYPE",
	getNextState
});

export default createAction;
