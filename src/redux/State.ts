import Data, {
	Car as JsonDataCar,
	TrafficLight as JsonTrafficLight
} from "../Data";

export interface TrafficLight extends JsonTrafficLight {
	green: boolean;
}

export interface Car extends JsonDataCar {
	position: number;
	isSelected: boolean;
	finishPosition?: number;
}

interface State extends Data {
	cars: Car[];
	isLoading: boolean;
	searchValue: string;
	animationSpeed: number;
	started: boolean;
	traffic_lights: TrafficLight[];
}

export const initialState: State = {
	cars: [],
	isLoading: true,
	searchValue: "",
	distance: 0,
	speed_limits: [],
	traffic_lights: [],
	animationSpeed: 1,
	started: false
};

export default State;
