import * as React from "react";
import * as ReactDOM from "react-dom";
import { createStore } from "redux";
import reducer from "./redux/reducer";
import State from "./redux/State";
import { Provider } from "react-redux";
import Main from "./components/Main";
import { startRace, stopRace } from "./race/startStop";

const testState: State = {
	distance: 500,
	cars: [
		{
			name: "test",
			description: "test",
			id: 1,
			image: "search.svg",
			isSelected: true,
			position: 0,
			speed: 100
		}
	],
	speed_limits: [{ position: 400, speed: 89 }],
	traffic_lights: [{ position: 100, duration: 5000, green: false }],
	animationSpeed: 3,
	isLoading: false,
	searchValue: "test",
	started: false
};

it("renders without crashing", () =>
	new Promise(resolve => {
		const store = createStore(reducer, testState);
		const div = document.createElement("div");
		ReactDOM.render(
			<Provider store={store}>
				<Main />
			</Provider>,
			div
		);

		startRace(store.dispatch);

		window.setTimeout(() => {
			stopRace(store.dispatch);
			ReactDOM.unmountComponentAtNode(div);
			resolve();
		}, 2000);
	}));
