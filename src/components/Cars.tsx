import * as React from "react";
import { connect } from "react-redux";

import State, { Car } from "../redux/State";
import splitArrayIntoChunks from "../splitArrayIntoChunks";
import { Dispatch } from "redux";
import createAction from "../redux/actions/createAction";
import CarSelectionRow from "./CarSelectionRow";

const DEFAULT_NUMBER_OF_CARS_IN_ROW = 3;

interface Properties {
	cars: Car[];
	numberOfCarsInRow?: number;
	searchValue: string;
	onCarClicked: (id: number) => void;
}

const Cars: React.SFC<Properties> = ({
	cars,
	numberOfCarsInRow = DEFAULT_NUMBER_OF_CARS_IN_ROW,
	searchValue,
	onCarClicked
}) => {
	const lowerCaseSearch = searchValue.toLocaleLowerCase();
	const filterdCars = cars.filter(
		car => car.name.toLocaleLowerCase().indexOf(lowerCaseSearch) > -1
	);
	const chunks = splitArrayIntoChunks(filterdCars, numberOfCarsInRow);

	return (
		<div>
			{chunks.map((row, index) => (
				<CarSelectionRow
					key={index}
					cars={row}
					isTopBorderDisabled={index === 0}
					numberOfCarsInRow={numberOfCarsInRow}
					isOnlyRow={chunks.length === 1}
					onCarClicked={onCarClicked}
				/>
			))}
		</div>
	);
};

const stateToProps = ({ cars, searchValue }: State) => ({
	cars,
	searchValue
});

const dispatchToProps = (dispatch: Dispatch) => ({
	onCarClicked: (id: number) =>
		dispatch(
			createAction((state: State) => {
				const newCars = state.cars.slice();
				const index = newCars.findIndex(car => car.id === id);
				const car = state.cars[index];
				newCars[index] = { ...car, isSelected: !car.isSelected };

				return {
					...state,
					cars: newCars
				};
			})
		)
});

export default connect(
	stateToProps,
	dispatchToProps
)(Cars);
