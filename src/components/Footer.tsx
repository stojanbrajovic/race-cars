import * as React from "react";
import { bordered } from "./styles";
import State from "../redux/State";
import { Dispatch } from "redux";
import setState from "../redux/actions/setState";
import { connect } from "react-redux";
import { startRace, stopRace } from "../race/startStop";

const elementStyle: React.CSSProperties = {
	...bordered,
	width: 200,
	marginLeft: 20
};

interface Properties {
	animationSpeed: number;
	onValueChange: React.ChangeEventHandler<HTMLInputElement>;
	started: boolean;
	startRace: VoidFunction;
	stopRace: VoidFunction;
	isStartDisabled: boolean;
}

const Footer: React.SFC<Properties> = ({
	animationSpeed,
	onValueChange,
	startRace,
	stopRace,
	started,
	isStartDisabled
}) => (
	<div style={{ display: "flex", justifyContent: "flex-end", height: 50 }}>
		<input
			style={{ ...elementStyle, padding: 5 }}
			type="number"
			onChange={onValueChange}
			value={animationSpeed || ""}
			placeholder="Animation speed"
			disabled={started}
		/>
		<button
			style={{
				...elementStyle,
				cursor: isStartDisabled ? "not-allowed" : "pointer"
			}}
			disabled={isStartDisabled}
			onClick={started ? stopRace : startRace}
		>
			{started ? "RESET" : "START"}
		</button>
	</div>
);

const stateToProps = ({ animationSpeed, started, cars }: State) => ({
	animationSpeed,
	started,
	isStartDisabled:
		!animationSpeed || !cars.filter(car => car.isSelected).length
});

const dispatchToProps = (dispatch: Dispatch) => ({
	onValueChange: (event: React.ChangeEvent<HTMLInputElement>) => {
		const animationSpeed = parseFloat(event.currentTarget.value) || 0;
		setState(dispatch, { animationSpeed });
	},
	startRace: () => startRace(dispatch),
	stopRace: () => stopRace(dispatch)
});

export default connect(
	stateToProps,
	dispatchToProps
)(Footer);
