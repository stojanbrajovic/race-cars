import * as React from "react";
import { connect } from "react-redux";
import Cars from "./Cars";
import State from "../redux/State";
import Search from "./Search";
import RaceTrack from "./RaceTrack";
import Footer from "./Footer";

interface Properties {
	isLoading: boolean;
}

const Main: React.SFC<Properties> = ({ isLoading }) =>
	isLoading ? (
		<span>Loading...</span>
	) : (
		<div style={{ margin: 50 }}>
			<Search />
			<Cars />
			<RaceTrack />
			<Footer />
		</div>
	);

const stateToProps = ({ isLoading }: State) => ({
	isLoading
});

export default connect(stateToProps)(Main);
