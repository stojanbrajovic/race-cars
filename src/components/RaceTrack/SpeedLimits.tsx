import * as React from "react";
import { connect } from "react-redux";
import { SpeedLimit } from "../../Data";
import State from "../../redux/State";
import { BORDER_WIDTH, TRAFFIC_SIGN_HEIGHT } from "../styles";
import TrafficSign from "./TrafficSign";

const SIGN_BORDER_WIDTH = 10;

interface Properties {
	distance: number;
	speedLimits: SpeedLimit[];
}

const SpeedLimits: React.SFC<Properties> = ({ speedLimits, distance }) => (
	<>
		{speedLimits.map(limit => (
			<TrafficSign
				distance={distance}
				position={limit.position}
				key={limit.position}
			>
				<div
					style={{
						position: "absolute",
						top: "100%",
						left:
							-TRAFFIC_SIGN_HEIGHT / 2 -
							SIGN_BORDER_WIDTH +
							BORDER_WIDTH,
						borderColor: "grey",
						borderRadius: TRAFFIC_SIGN_HEIGHT - SIGN_BORDER_WIDTH,
						width: TRAFFIC_SIGN_HEIGHT - SIGN_BORDER_WIDTH,
						height: TRAFFIC_SIGN_HEIGHT - SIGN_BORDER_WIDTH,
						borderWidth: SIGN_BORDER_WIDTH,
						borderStyle: "solid"
					}}
				>
					<div
						style={{
							display: "flex",
							justifyContent: "center",
							alignItems: "center",
							height: "100%",
							fontSize: TRAFFIC_SIGN_HEIGHT / 2
						}}
					>
						{limit.speed}
					</div>
				</div>
			</TrafficSign>
		))}
	</>
);

const stateToProps = (state: State) => ({
	speedLimits: state.speed_limits,
	distance: state.distance
});

export default connect(stateToProps)(SpeedLimits);
