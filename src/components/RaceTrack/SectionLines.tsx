import * as React from "react";
import { BORDER_WIDTH } from "../styles";

interface Properties {
	numberOfSections: number;
}

const SectionLines: React.SFC<Properties> = ({ numberOfSections }) => (
	<div
		style={{
			display: "flex",
			justifyContent: "space-evenly",
			height: "100%",
			width: "100%",
			position: "absolute"
		}}
	>
		{Array(numberOfSections - 1)
			.fill(0)
			.map((_, index) => (
				<div
					key={index}
					style={{
						width: BORDER_WIDTH,
						backgroundColor: "grey"
					}}
				/>
			))}
	</div>
);

export default SectionLines;
