import * as React from "react";
import { TRACK_ROW_HEIGHT } from "../styles";
import CarRow from "./CarRow";
import { Car } from "../../redux/State";

interface Properties {
	cars: Car[];
}

const SelectedCars: React.SFC<Properties> = ({ cars }) => (
	<div
		style={{
			position: "absolute",
			height: cars.length * TRACK_ROW_HEIGHT,
			width: "100%"
		}}
	>
		{cars.map((car, index) => (
			<CarRow car={car} key={car.id} showTopBorder={index !== 0} />
		))}
	</div>
);

export default SelectedCars;
