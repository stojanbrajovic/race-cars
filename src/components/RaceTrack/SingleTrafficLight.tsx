import * as React from "react";
import TrafficSign from "./TrafficSign";
import { TrafficLight } from "../../redux/State";
import {
	BORDER_RADIUS,
	BLACK_BORDER,
	BORDER_WIDTH,
	TRAFFIC_SIGN_HEIGHT
} from "../styles";

interface Properties {
	trafficLight: TrafficLight;
	distance: number;
}

const LIGHT_SIGN_WIDTH = 60;
const LIGHT_RADIUS = 35;
const LIGHT_OFF_COLOR = "lightgrey";

const SingleTrafficLight: React.SFC<Properties> = ({
	trafficLight,
	distance
}) => {
	const renderTrafficLightSign = () => (
		<div
			style={{
				position: "absolute",
				top: "100%",
				left: -LIGHT_SIGN_WIDTH / 2 - BORDER_WIDTH,
				border: BLACK_BORDER,
				borderRadius: BORDER_RADIUS,
				width: LIGHT_SIGN_WIDTH,
				height: TRAFFIC_SIGN_HEIGHT
			}}
		>
			{renderSingleLight(trafficLight.green ? LIGHT_OFF_COLOR : "red")}
			{renderSingleLight(trafficLight.green ? "green" : LIGHT_OFF_COLOR)}
		</div>
	);

	const renderSingleLight = (color: string) => (
		<div
			style={{
				display: "flex",
				justifyContent: "center",
				alignItems: "center",
				height: "50%",
				width: "100%"
			}}
		>
			<div
				style={{
					height: LIGHT_RADIUS,
					width: LIGHT_RADIUS,
					borderRadius: LIGHT_RADIUS,
					backgroundColor: color
				}}
			/>
		</div>
	);

	return (
		<TrafficSign position={trafficLight.position} distance={distance}>
			{renderTrafficLightSign()}
		</TrafficSign>
	);
};

export default SingleTrafficLight;
