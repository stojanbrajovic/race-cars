import * as React from "react";

interface Properties {
	distance: number;
	numberOfSections: number;
}

const SectionsText: React.SFC<Properties> = ({
	distance,
	numberOfSections
}) => (
	<div
		style={{
			display: "flex",
			justifyContent: "space-evenly",
			marginBottom: 5
		}}
	>
		{Array(numberOfSections - 1)
			.fill(0)
			.map((_, index) => (
				<span key={index}>
					{index + 1}x{Math.round(distance / numberOfSections)}
				</span>
			))}
	</div>
);

export default SectionsText;
