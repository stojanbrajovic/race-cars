import State from "../../../redux/State";
import { Car } from "../../../redux/State";

export interface OwnProps {
	car: Car;
	showTopBorder: boolean;
}

const positionToColor = (position = 0) => {
	switch (position) {
		case 1:
			return "rgba(255,242,0,0.5)";
		case 2:
			return "rgba(192,192,192,0.5)";
		case 3:
			return "rgba(205,127,50,0.5)";
		default:
			return "rgba(0,0,0,0)";
	}
};

const stateToProps = ({ started, distance }: State, { car }: OwnProps) => ({
	started,
	overlayColor: positionToColor(car.finishPosition),
	position: car.finishPosition || 0,
	distance
});

export default stateToProps;
