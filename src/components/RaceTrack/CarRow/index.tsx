import * as React from "react";
import { connect } from "react-redux";
import ImageDiv from "../../ImageDiv";
import {
	TRACK_ROW_HEIGHT,
	BLACK_BORDER,
	TRANSPARENT_BORDER,
	BORDER_WIDTH
} from "../../styles";
import stateToProps, { OwnProps } from "./stateToProps";
import { Car } from "../../../redux/State";

interface Properties extends OwnProps {
	car: Car;
	showTopBorder: boolean;
	overlayColor: string;
	started: boolean;
	distance: number;
}

const IMAGE_SIZE = 0.8 * TRACK_ROW_HEIGHT;

const getImageStyle = (
	car: Car,
	started: boolean,
	overlayColor: string,
	distance: number
) => {
	const style: React.CSSProperties = {
		height: IMAGE_SIZE,
		width: IMAGE_SIZE,
		boxShadow: `inset 0 0 0 ${IMAGE_SIZE}px ${overlayColor}`,
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		fontSize: IMAGE_SIZE * 0.8,
		position: "relative"
	};

	if (car.finishPosition || !started) {
		style.margin = (TRACK_ROW_HEIGHT - IMAGE_SIZE) / 2;
	} else {
		style.left = `calc(${(car.position / distance) *
			100}% - ${IMAGE_SIZE}px)`;
	}

	return style;
};

const CarRow: React.SFC<Properties> = ({
	car,
	showTopBorder,
	started,
	overlayColor,
	distance
}) => (
	<div
		style={{
			height: TRACK_ROW_HEIGHT - BORDER_WIDTH,
			borderTop: showTopBorder ? BLACK_BORDER : TRANSPARENT_BORDER,
			width: "100%",
			display: "flex",
			alignItems: "center",
			justifyContent: car.finishPosition ? "flex-end" : "flex-start",
			overflow: "hidden"
		}}
	>
		<ImageDiv
			imageUrl={car.image}
			style={getImageStyle(car, started, overlayColor, distance)}
		>
			{car.finishPosition || ""}
		</ImageDiv>
	</div>
);

export default connect(stateToProps)(CarRow);
