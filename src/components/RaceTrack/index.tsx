import * as React from "react";
import {
	BLACK_BORDER,
	BORDER_RADIUS,
	TRACK_ROW_HEIGHT,
	TRAFFIC_SIGN_HEIGHT,
	TRAFFIC_SIGN_LINE_ADDED_HEIGHT
} from "../styles";
import State, { Car } from "../../redux/State";
import { connect } from "react-redux";
import SectionsText from "./SectionsText";
import SectionLines from "./SectionLines";
import SpeedLimits from "./SpeedLimits";
import TrafficLights from "./TrafficLights";
import SelectedCars from "./SelectedCars";

interface Properties {
	distance: number;
	selectedCars: Car[];
	numberOfSections?: number;
}

const ADDITIONAL_HEIGHT = 80;

const RaceTrack: React.SFC<Properties> = ({
	distance,
	numberOfSections = 1,
	selectedCars
}) => (
	<div
		style={{
			marginTop: 30,
			height:
				(selectedCars.length || 1) * TRACK_ROW_HEIGHT +
				TRAFFIC_SIGN_HEIGHT +
				TRAFFIC_SIGN_LINE_ADDED_HEIGHT +
				ADDITIONAL_HEIGHT
		}}
	>
		<SectionsText distance={distance} numberOfSections={numberOfSections} />
		<div
			style={{
				position: "relative",
				minHeight: TRACK_ROW_HEIGHT,
				height: selectedCars.length * TRACK_ROW_HEIGHT
			}}
		>
			<div
				style={{
					border: BLACK_BORDER,
					borderRadius: BORDER_RADIUS,
					height: "100%",
					width: "100%",
					position: "absolute"
				}}
			>
				<SelectedCars cars={selectedCars} />
				<SpeedLimits />
				<TrafficLights />
				<SectionLines numberOfSections={numberOfSections} />
			</div>
		</div>
	</div>
);
RaceTrack.defaultProps = {
	numberOfSections: 10
};

const stateToProps = ({ distance, cars }: State) => ({
	distance,
	selectedCars: cars.filter(car => car.isSelected)
});

export default connect(stateToProps)(RaceTrack);
