import * as React from "react";
import { BORDER_WIDTH, TRAFFIC_SIGN_LINE_ADDED_HEIGHT } from "../styles";

const HEIGHT_ABOVE_TRACK = 8;

interface Properties {
	distance: number;
	position: number;
}

const TrafficSign: React.SFC<Properties> = ({
	position,
	distance,
	children
}) => (
	<div
		key={position}
		style={{
			height: `calc(100% + ${TRAFFIC_SIGN_LINE_ADDED_HEIGHT}px)`,
			position: "absolute",
			left: `calc(${Math.round(
				(100 * position) / distance
			)}% - ${BORDER_WIDTH / 2}px)`,
			top: -HEIGHT_ABOVE_TRACK,
			borderStyle: "dashed",
			borderBottom: 0,
			borderRight: 0,
			borderTop: 0,
			borderColor: "grey",
			borderWidth: BORDER_WIDTH * 2,
			width: 0,
			overflow: "visible"
		}}
	>
		{children}
	</div>
);

export default TrafficSign;
