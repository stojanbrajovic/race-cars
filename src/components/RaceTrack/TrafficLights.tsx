import * as React from "react";
import { TrafficLight } from "../../redux/State";
import SingleTrafficLight from "./SingleTrafficLight";
import State from "../../redux/State";
import { connect } from "react-redux";

interface Properties {
	trafficLights: TrafficLight[];
	distance: number;
}

const TrafficLights: React.SFC<Properties> = ({ trafficLights, distance }) => (
	<>
		{trafficLights.map(light => (
			<SingleTrafficLight
				trafficLight={light}
				distance={distance}
				key={light.position}
			/>
		))}
	</>
);

const stateToProps = ({ traffic_lights, distance }: State) => ({
	trafficLights: traffic_lights,
	distance
});

export default connect(stateToProps)(TrafficLights);
