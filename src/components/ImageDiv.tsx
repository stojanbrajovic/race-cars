import * as React from "react";
import { BORDER_RADIUS, BLACK_BORDER } from "./styles";

interface Properties {
	imageUrl: string;
	style?: React.CSSProperties;
}

const ImageDiv: React.SFC<Properties> = props => (
	<div
		style={{
			backgroundImage: `url(${props.imageUrl})`,
			backgroundSize: "contain",
			backgroundRepeat: "no-repeat",
			backgroundPosition: "center",
			borderRadius: BORDER_RADIUS,
			border: BLACK_BORDER,
			...props.style
		}}
	>
		{props.children}
	</div>
);
ImageDiv.defaultProps = {
	style: {}
};

export default ImageDiv;
