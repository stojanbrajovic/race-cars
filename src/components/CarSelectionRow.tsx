import * as React from "react";
import { Car } from "../redux/State";
import { TRANSPARENT_BORDER, BLACK_BORDER } from "./styles";
import SingleCar from "./SingleCar";

const CAR_HEIGHT = 100;
const CAR_WIDTH = 200;

interface Properties {
	cars: Car[];
	isTopBorderDisabled: boolean;
	numberOfCarsInRow: number;
	isOnlyRow: boolean;
	onCarClicked: (id: number) => void;
}

const CarSelectionRow: React.SFC<Properties> = ({
	cars,
	numberOfCarsInRow,
	isTopBorderDisabled,
	isOnlyRow,
	onCarClicked
}) => {
	const emptyCells = [];
	while (emptyCells.length + cars.length < numberOfCarsInRow) {
		emptyCells.push(
			<div
				style={{
					width: `${100 / numberOfCarsInRow}%`,
					borderLeft: isOnlyRow ? TRANSPARENT_BORDER : BLACK_BORDER
				}}
				key={emptyCells.length}
			/>
		);
	}

	return (
		<div
			style={{
				display: "flex",
				justifyContent: "space-between",
				borderTop: isTopBorderDisabled ? "" : BLACK_BORDER
			}}
		>
			{cars.map((car, index) => (
				<div
					key={car.id}
					style={{
						width: `${100 / numberOfCarsInRow}%`,
						borderLeft: index === 0 ? "" : BLACK_BORDER,
						display: "flex",
						justifyContent: "center",
						paddingTop: 10,
						paddingBottom: 10
					}}
				>
					<SingleCar
						car={car}
						width={CAR_WIDTH}
						height={CAR_HEIGHT}
						onClick={onCarClicked.bind(null, car.id)}
					/>
				</div>
			))}
			{emptyCells}
		</div>
	);
};

export default CarSelectionRow;
