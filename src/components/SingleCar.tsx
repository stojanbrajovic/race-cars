import * as React from "react";
import { Car } from "../redux/State";
import ImageDiv from "./ImageDiv";
import { BORDER_WIDTH } from "./styles";

const getContainerDivStyle = (isFlipped: boolean): React.CSSProperties => ({
	width: "100%",
	height: "100%",
	position: "relative",
	cursor: "pointer",
	transform: isFlipped ? "rotateX(180deg)" : "",
	transition: "0.6s",
	transformStyle: "preserve-3d"
});

const getCardStyle = (): React.CSSProperties => ({
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
	backfaceVisibility: "hidden",
	position: "absolute",
	width: "100%",
	height: `calc(100% - ${2 * BORDER_WIDTH}px)`,
	top: 0,
	left: 0
});

interface Properties {
	car: Car;
	width: number;
	height: number;
	onClick: VoidFunction;
}

interface State {
	isMouseInside: boolean;
}

class SingleCar extends React.Component<Properties, State> {
	state: State = { isMouseInside: false };

	render() {
		const { width, height, onClick, car } = this.props;
		return (
			<div
				style={{
					width,
					height,
					cursor: "pointer"
				}}
				onClick={onClick}
				onMouseEnter={() => this.setState({ isMouseInside: true })}
				onMouseLeave={() => this.setState({ isMouseInside: false })}
			>
				<div
					style={getContainerDivStyle(
						!car.isSelected && this.state.isMouseInside
					)}
				>
					<ImageDiv
						imageUrl={car.image}
						style={{
							...getCardStyle(),
							zIndex: 2
						}}
					>
						<div
							style={{
								backgroundColor: "white",
								color: car.isSelected ? "green" : ""
							}}
						>
							{car.isSelected ? <span>&#9745;</span> : null}
							{car.name}
						</div>
					</ImageDiv>
					<ImageDiv
						imageUrl={car.image}
						style={{
							...getCardStyle(),
							transform: "rotateX(180deg)",
							boxShadow: `inset 0 0 0 ${Math.max(
								width,
								height
							)}px rgba(0,0,0,0.5)`
						}}
					>
						<div style={{ backgroundColor: "white", fontSize: 10 }}>
							<div>{car.description}</div>
							<div>Speed: {car.speed}</div>
						</div>
					</ImageDiv>
				</div>
			</div>
		);
	}
}

export default SingleCar;
