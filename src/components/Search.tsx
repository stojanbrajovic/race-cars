import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import State from "../redux/State";
import setState from "../redux/actions/setState";
import { BLACK_BORDER, BORDER_RADIUS } from "./styles";
import ImageDiv from "./ImageDiv";

interface Properties {
	value: string;
	updateValue: (value: string) => void;
}

const Search: React.SFC<Properties> = ({ value, updateValue }) => {
	const onChange = (event: React.ChangeEvent<HTMLInputElement>) =>
		updateValue(event.currentTarget.value);

	return (
		<div
			style={{
				marginBottom: 40,
				display: "flex"
			}}
		>
			<input
				style={{
					width: "100%",
					border: BLACK_BORDER,
					borderTopLeftRadius: BORDER_RADIUS,
					borderBottomLeftRadius: BORDER_RADIUS,
					borderRight: 0,
					padding: 10,
					outline: "none"
				}}
				value={value}
				onChange={onChange}
			/>
			<ImageDiv
				imageUrl="search.svg"
				style={{
					width: 30,
					border: BLACK_BORDER,
					borderTopRightRadius: BORDER_RADIUS,
					borderBottomRightRadius: BORDER_RADIUS,
					borderTopLeftRadius: 0, // ???
					borderBottomLeftRadius: 0,
					padding: 7,
					backgroundOrigin: "content-box"
				}}
			/>
		</div>
	);
};

const stateToProps = ({ searchValue }: State) => ({
	value: searchValue
});

const dispatchToProps = (dispatch: Dispatch) => ({
	updateValue: (searchValue: string) => setState(dispatch, { searchValue })
});

export default connect(
	stateToProps,
	dispatchToProps
)(Search);
