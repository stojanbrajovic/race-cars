export const BORDER_WIDTH = 2;
export const BLACK_BORDER = `${BORDER_WIDTH}px solid black`;
export const TRANSPARENT_BORDER = `${BORDER_WIDTH}px solid rgba(0,0,0,0)`;
export const BORDER_RADIUS = 10;
export const TRAFFIC_SIGN_HEIGHT = 100;
export const TRAFFIC_SIGN_LINE_ADDED_HEIGHT = 50;
export const TRACK_ROW_HEIGHT = 80;

export const bordered: React.CSSProperties = {
	border: BLACK_BORDER,
	borderRadius: BORDER_RADIUS,
	outline: "none"
};
