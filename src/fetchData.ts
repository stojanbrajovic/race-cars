import Data from "./Data";

const fetchData = async () =>
	new Promise<Data>(resolve => {
		const xobj = new XMLHttpRequest();
		xobj.open("GET", "data.json", true); // Replace 'my_data' with the path to your file
		xobj.onreadystatechange = () => {
			if (xobj.readyState === 4 && xobj.status === 200) {
				resolve(JSON.parse(xobj.responseText));
			}
		};

		xobj.send();
	});

export default fetchData;
