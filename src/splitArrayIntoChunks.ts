const splitArrayIntoChunks = <Type>(array: Type[], chunkLength: number) => {
	const result: Type[][] = [];
	if (array.length === 0) {
		return result;
	}

	do {
		const numChunksElements = result.length * chunkLength;
		result.push(
			array.slice(numChunksElements, numChunksElements + chunkLength)
		);
	} while (result.length * chunkLength < array.length);

	return result;
};

export default splitArrayIntoChunks;
