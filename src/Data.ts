export interface Car {
	id: number;
	image: string;
	speed: number;
	description: string;
	name: string;
}

export interface SpeedLimit {
	position: number;
	speed: number;
}

export interface TrafficLight {
	position: number;
	duration: number;
}

export interface Data {
	cars: Car[];
	distance: number;
	speed_limits: SpeedLimit[];
	traffic_lights: TrafficLight[];
}

export default Data;
